# hello-python

Dependencies:

* Python 3

Install the requirements:

```
pip install -r requirements.txt
```

Configure the application:

```
export RABBITMQ_HOST = [ rabbitmq host address ]
export RABBITMQ_PORT = [ rabbitmq port ]
export RABBITMQ_QUEUE = hello
```

Run the app localy:

```
python app.py
```
